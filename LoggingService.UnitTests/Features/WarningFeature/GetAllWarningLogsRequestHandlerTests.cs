﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using LoggingService.Features.WarningFeature;
using Moq;
using NUnit.Framework;

namespace LoggingService.UnitTests.Features.WarningFeature
{
    public class GetAllWarningLogsRequestHandlerTests
    {
        private Mock<IRepository<Warning>> _warningRepository;

        private GetAllWarningLogsRequestHandler _objectToTest;
        [SetUp]
        public void Setup()
        {
            _warningRepository = new Mock<IRepository<Warning>>();

            _warningRepository.Setup(x => x.Read()).ReturnsAsync(new List<Warning>
            {
                new ("Test", "Test", DateTime.Now){Identifier = "IdentifierExact", WarningText = string.Empty},
                new ("Test", "SummaryExact", DateTime.Now){Identifier = "Test", WarningText = string.Empty},
                new ("Test", "Test", DateTime.Now.AddDays(1)){Identifier = "Test", WarningText = string.Empty},
                new ("Test", "Test", DateTime.Now.AddDays(2)){Identifier = "Test", WarningText = string.Empty},
                new ("Test", "Test", DateTime.Now.AddDays(-1)){Identifier = "Test", WarningText = string.Empty},
                new ("Test", "Test", DateTime.Now){Identifier = "Test", WarningText = "WarningExact"},
            });


            _objectToTest = new GetAllWarningLogsRequestHandler(_warningRepository.Object);
        }
        [Test]
        public async Task IdentifierExactFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllWarningLogsRequest
            {
                ClientId = "Test",
                Identifier = "IdentifierExact",
                IdentifierExact = true
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task SummaryExactFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllWarningLogsRequest
            {
                ClientId = "Test",
                Summary = "SummaryExact",
                SummaryExact = true
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task LowerDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllWarningLogsRequest
            {
                ClientId = "Test",
                LowerBoundDateTime = DateTime.Now.AddDays(1).AddHours(1)
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task UpperDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllWarningLogsRequest
            {
                ClientId = "Test",
                UpperBoundDateTime = DateTime.Now.AddHours(-1)
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task UpperAndLowerDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllWarningLogsRequest
            {
                ClientId = "Test",
                UpperBoundDateTime = DateTime.Now.AddHours(1).AddDays(2),
                LowerBoundDateTime = DateTime.Now.AddHours(1)
            });

            Assert.That(result.Count, Is.EqualTo(2));
        }

        [Test]
        public async Task WarningExactFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllWarningLogsRequest
            {
                ClientId = "Test",
                WarningText = "WarningExact",
                WarningTextExact = true
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }
    }
}
