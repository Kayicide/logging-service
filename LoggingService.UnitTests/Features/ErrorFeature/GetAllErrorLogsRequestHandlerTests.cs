﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using LoggingService.Features.ErrorFeature;
using Moq;
using NUnit.Framework;

namespace LoggingService.UnitTests.Features.ErrorFeature
{
    public class GetAllErrorLogsRequestHandlerTests
    {
        private Mock<IRepository<Error>> _errorRepository;

        private GetAllErrorLogsRequestHandler _objectToTest;
        [SetUp]
        public void Setup()
        {
            _errorRepository = new Mock<IRepository<Error>>();
            _errorRepository.Setup(x => x.Read()).ReturnsAsync(new List<Error>
            {
                new Error("Test", "Test", DateTime.Now){Identifier = "IdentifierExact", Severity = Severity.Minor, StackTrace = string.Empty},
                new Error("Test", "SummaryExact", DateTime.Now){Identifier = "Test", Severity = Severity.Minor, StackTrace = string.Empty},
                new Error("Test", "Test", DateTime.Now.AddDays(1)){Identifier = "Test", Severity = Severity.Minor, StackTrace = string.Empty},
                new Error("Test", "Test", DateTime.Now.AddDays(2)){Identifier = "Test", Severity = Severity.Minor, StackTrace = string.Empty},
                new Error("Test", "Test", DateTime.Now.AddDays(-1)){Identifier = "Test", Severity = Severity.Minor, StackTrace = string.Empty},
                new Error("Test", "Test", DateTime.Now){Identifier = "Test", Severity = Severity.Critical, StackTrace = string.Empty},
                new Error("Test", "Test", DateTime.Now){Identifier = "Test", Severity = Severity.Minor, StackTrace = "Object not set to instance of an object!"},
            });

            _objectToTest = new GetAllErrorLogsRequestHandler(_errorRepository.Object);
        }
        [Test]
        public async Task IdentifierExactFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllErrorLogsRequest
            {
                ClientId = "Test",
                Identifier = "IdentifierExact",
                IdentifierExact = true
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task SummaryExactFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllErrorLogsRequest
            {
                ClientId = "Test",
                Summary = "SummaryExact",
                SummaryExact = true
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task LowerDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllErrorLogsRequest
            {
                ClientId = "Test",
                LowerBoundDateTime = DateTime.Now.AddDays(1).AddHours(1)
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task UpperDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllErrorLogsRequest
            {
                ClientId = "Test",
                UpperBoundDateTime = DateTime.Now.AddHours(-1)
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task UpperAndLowerDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllErrorLogsRequest
            {
                ClientId = "Test",
                UpperBoundDateTime = DateTime.Now.AddHours(1).AddDays(2),
                LowerBoundDateTime = DateTime.Now.AddHours(1)
            });

            Assert.That(result.Count, Is.EqualTo(2));
        }

        [Test]
        public async Task SeverityFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllErrorLogsRequest
            {
                ClientId = "Test",
                Severity = Severity.Critical
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task StackTraceExactFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllErrorLogsRequest
            {
                ClientId = "Test",
                StackTrace = "Object not set to instance of an object!",
                StackTraceExact = true
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }
    }
}
