﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using LoggingService.Features.InformationFeature;
using Moq;
using NUnit.Framework;

namespace LoggingService.UnitTests.Features.InformationFeature
{
    public class GetAllInfoLogsRequestHandlerTests
    {
        private Mock<IRepository<Information>> _infoRepository;

        private GetAllInfoLogsRequestHandler _objectToTest;

        [SetUp]
        public void SetUp()
        {
            _infoRepository = new Mock<IRepository<Information>>();

            _infoRepository.Setup(x => x.Read()).ReturnsAsync(new List<Information>
            {
                new ("Test", "Test", DateTime.Now){Identifier = "IdentifierExact", InfoText = string.Empty},
                new ("Test", "SummaryExact", DateTime.Now){Identifier = "Test", InfoText = string.Empty},
                new ("Test", "Test", DateTime.Now.AddDays(1)){Identifier = "Test", InfoText = string.Empty},
                new ("Test", "Test", DateTime.Now.AddDays(2)){Identifier = "Test", InfoText = string.Empty},
                new ("Test", "Test", DateTime.Now.AddDays(-1)){Identifier = "Test", InfoText = string.Empty},
                new ("Test", "Test", DateTime.Now){Identifier = "Test", InfoText = "InfoExact"},
            });

            _objectToTest = new GetAllInfoLogsRequestHandler(_infoRepository.Object);
        }

        [Test]
        public async Task IdentifierExactFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllInfoLogsRequest
            {
                ClientId = "Test",
                Identifier = "IdentifierExact",
                IdentifierExact = true
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task SummaryExactFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllInfoLogsRequest
            {
                ClientId = "Test",
                Summary = "SummaryExact",
                SummaryExact = true
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task LowerDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllInfoLogsRequest
            {
                ClientId = "Test",
                LowerBoundDateTime = DateTime.Now.AddDays(1).AddHours(1)
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task UpperDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllInfoLogsRequest
            {
                ClientId = "Test",
                UpperBoundDateTime = DateTime.Now.AddHours(-1)
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task UpperAndLowerDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllInfoLogsRequest
            {
                ClientId = "Test",
                UpperBoundDateTime = DateTime.Now.AddHours(1).AddDays(2),
                LowerBoundDateTime = DateTime.Now.AddHours(1)
            });

            Assert.That(result.Count, Is.EqualTo(2));
        }

        [Test]
        public async Task InfoExactFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllInfoLogsRequest
            {
                ClientId = "Test",
                InfoText = "InfoExact",
                InfoTextExact = true
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }
    }
}
