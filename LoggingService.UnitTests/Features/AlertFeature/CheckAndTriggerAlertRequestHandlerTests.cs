﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EllipticCurve;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using LoggingService.Features;
using LoggingService.Features.AlertFeature;
using LoggingService.Features.LogFeature;
using LoggingService.Utility;
using Moq;
using NUnit.Framework;

namespace LoggingService.UnitTests.Features.AlertFeature
{
    public class CheckAndTriggerAlertRequestHandlerTests
    {
        private Mock<IRepository<Alert>> _alertRepository;
        private Mock<IRepository<User>> _userRepository;
        private Mock<IRequestHandler<GetAllLogsRequest, IList<Log>>> _getAllLogsRequestHandler;
        private AppSettings _appSettings;

        private CheckAndTriggerAlertRequestHandler _objectToTest;

        [SetUp]
        public void Setup()
        {
            _alertRepository = new Mock<IRepository<Alert>>();
            _userRepository = new Mock<IRepository<User>>();
            _getAllLogsRequestHandler = new Mock<IRequestHandler<GetAllLogsRequest, IList<Log>>>();
            _appSettings = new AppSettings {MailGridAuthKey = "UnitTest"};

            _userRepository.Setup(x => x.Read()).ReturnsAsync(new List<User>{new User{Email = "Email", FirstName = "First", LastName = "Last", ClientIds = new List<string>{"Test"}}});

            _objectToTest = new CheckAndTriggerAlertRequestHandler(_alertRepository.Object, _userRepository.Object, _getAllLogsRequestHandler.Object, _appSettings);
        }

        [Test]
        public async Task AlertIsNotTriggerWhenBelowTriggerDifferentTypes()
        {
            bool alertUpdated = false;

            _alertRepository.Setup(x => x.Read()).ReturnsAsync(new List<Alert>{new Alert{ClientId = "Test", TimeSpan = 60, LogType = "Error", ThresholdAmount = 2}});
            _alertRepository.Setup(x => x.Update(It.IsAny<Alert>())).Callback((Alert alert) =>
            {
                alertUpdated = true;
            });
            _getAllLogsRequestHandler.Setup(x => x.HandleRequest(It.IsAny<GetAllLogsRequest>())).ReturnsAsync(new List<Log>{
                new Error("", "", DateTime.Now), 
                new Information("", "", DateTime.Now)
            });

            var result = await _objectToTest.HandleRequest(new CheckAndTriggerAlertRequest {ClientId = "Test"});

            Assert.That(result, Is.True);
            Assert.That(alertUpdated, Is.False);
        }

        [Test]
        public async Task AlertTriggeredWhenEqualToTrigger()
        {
            bool alertUpdated = false;

            _alertRepository.Setup(x => x.Read()).ReturnsAsync(new List<Alert> { new Alert { ClientId = "Test", TimeSpan = 60, LogType = "Error", ThresholdAmount = 2 } });
            _alertRepository.Setup(x => x.Update(It.IsAny<Alert>())).Callback((Alert alert) =>
            {
                alertUpdated = true;
            });
            _getAllLogsRequestHandler.Setup(x => x.HandleRequest(It.IsAny<GetAllLogsRequest>())).ReturnsAsync(new List<Log>{
                new Error("Test", "", DateTime.Now),
                new Error("Test", "", DateTime.Now)
            });

            var result = await _objectToTest.HandleRequest(new CheckAndTriggerAlertRequest { ClientId = "Test" });

            Assert.That(result, Is.True);
            Assert.That(alertUpdated, Is.True);
        }

        [Test]
        public async Task AlertIsNotTriggerWhenTriggeredRecently()
        {
            bool alertUpdated = false;

            _alertRepository.Setup(x => x.Read()).ReturnsAsync(new List<Alert> { new Alert { ClientId = "Test", TimeSpan = 60, LogType = "Error", ThresholdAmount = 2, LastTriggered = DateTime.Now.AddMinutes(-1)} });
            _alertRepository.Setup(x => x.Update(It.IsAny<Alert>())).Callback((Alert alert) =>
            {
                alertUpdated = true;
            });
            _getAllLogsRequestHandler.Setup(x => x.HandleRequest(It.IsAny<GetAllLogsRequest>())).ReturnsAsync(new List<Log>{
                new Error("Test", "", DateTime.Now),
                new Error("Test", "", DateTime.Now)
            });

            var result = await _objectToTest.HandleRequest(new CheckAndTriggerAlertRequest { ClientId = "Test" });

            Assert.That(result, Is.True);
            Assert.That(alertUpdated, Is.False);
        }
    }
}
