﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using LoggingService.Features.LogFeature;
using Moq;
using NUnit.Framework;

namespace LoggingService.UnitTests.Features.LogFeature
{
    public class GetAllLogsRequestHandlerTests
    {
        private Mock<IRepository<Log>> _logRepository;

        private GetAllLogsRequestHandler _objectToTest;
        [SetUp]
        public void Setup()
        {
            _logRepository = new Mock<IRepository<Log>>();

            _logRepository.Setup(x => x.Read()).ReturnsAsync(new List<Log>
            {
                new Error("Test", "Test", DateTime.Now){Identifier = "IdentifierExact"},
                new Error("Test", "SummaryExact", DateTime.Now){Identifier = "Test"},
                new Error("Test", "Test", DateTime.Now.AddDays(1)){Identifier = "Test"},
                new Error("Test", "Test", DateTime.Now.AddDays(2)){Identifier = "Test"},
                new Error("Test", "Test", DateTime.Now.AddDays(-1)){Identifier = "Test"},
            });

            _objectToTest = new GetAllLogsRequestHandler(_logRepository.Object);
        }

        [Test]
        public async Task IdentifierExactFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllLogsRequest
            {
                ClientId = "Test",
                Identifier = "IdentifierExact",
                IdentifierExact = true
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task SummaryExactFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllLogsRequest
            {
                ClientId = "Test",
                Summary = "SummaryExact",
                SummaryExact = true
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task LowerDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllLogsRequest
            {
                ClientId = "Test",
                LowerBoundDateTime = DateTime.Now.AddDays(1).AddHours(1)
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task UpperDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllLogsRequest
            {
                ClientId = "Test",
                UpperBoundDateTime = DateTime.Now.AddHours(-1)
            });

            Assert.That(result.Count, Is.EqualTo(1));
        }

        [Test]
        public async Task UpperAndLowerDateFiltersCorrectly()
        {
            var result = await _objectToTest.HandleRequest(new GetAllLogsRequest
            {
                ClientId = "Test",
                UpperBoundDateTime = DateTime.Now.AddHours(1).AddDays(2),
                LowerBoundDateTime = DateTime.Now.AddHours(1)
            });

            Assert.That(result.Count, Is.EqualTo(2));
        }
    }
}
