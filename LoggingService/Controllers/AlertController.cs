﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Controllers.HttpRequest;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using LoggingService.Features;
using LoggingService.Features.AlertFeature;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LoggingService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class AlertController : Controller
    {
        private readonly IRequestHandler<CreateAlertRequest, Alert> _createAlertRequestHandler;
        private readonly IRequestHandler<UpdateAlertRequest, Alert> _updateAlertRequestHandler;
        private readonly IRequestHandler<GetAllAlertsRequest, IList<Alert>> _getAllAlertsRequestHandler;
        public AlertController(IRequestHandler<CreateAlertRequest, Alert> createAlertRequestHandler, IRequestHandler<UpdateAlertRequest, Alert> updateAlertRequestHandler, IRequestHandler<GetAllAlertsRequest, IList<Alert>> getAllAlertsRequestHandler)
        {
            _createAlertRequestHandler = createAlertRequestHandler;
            _updateAlertRequestHandler = updateAlertRequestHandler;
            _getAllAlertsRequestHandler = getAllAlertsRequestHandler;
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> CreateAlert([FromBody] HttpPostCreateAlertRequest request)
        {
            var result = await _createAlertRequestHandler.HandleRequest(new CreateAlertRequest{ClientId = request.ClientId, Name = request.Name, TimeSpan = request.TimeSpan, LogIdentifier = request.LogIdentifier, LogType = request.LogType, ThresholdAmount = request.ThresholdAmount});
            if (result == null)
                return BadRequest($"Client with the ID: {request.ClientId} could not be found!");
            return Ok(request);
        } 

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> UpdateAlert([FromBody] HttpPostUpdateAlertRequest request)
        {
            var result = await _updateAlertRequestHandler.HandleRequest(new UpdateAlertRequest{AlertId = request.AlertId, Name = request.Name, TimeSpan = request.TimeSpan, Deleted = request.Deleted, LogIdentifier = request.LogIdentifier, LogType = request.LogType, ThresholdAmount = request.ThresholdAmount});
            if (result == null)
                return BadRequest($"Alert with the ID: {request.AlertId} could not be found!");
            return Ok(request);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAlerts([FromQuery] string clientId)
        {
            var response = await _getAllAlertsRequestHandler.HandleRequest(new GetAllAlertsRequest {ClientId = clientId});
            return Ok(response);
        }

    }
}
