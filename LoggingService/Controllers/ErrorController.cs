﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using LoggingService.Controllers.HttpRequest;
using LoggingService.Data.Entity;
using Microsoft.AspNetCore.Mvc;
using LoggingService.Features;
using LoggingService.Features.ErrorFeature;
using LoggingService.Utility;
using Microsoft.AspNetCore.Authorization;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LoggingService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class ErrorController : Controller
    {
        private readonly IRequestHandler<LogErrorRequest, ReturnObject> _logErrorRequestHandler;
        private readonly IRequestHandler<GetAllErrorLogsRequest, IList<Error>> _getAllErrorLogsRequestHandler;
        public ErrorController(IRequestHandler<LogErrorRequest, ReturnObject> logErrorRequestHandler, IRequestHandler<GetAllErrorLogsRequest, IList<Error>> getAllErrorLogsRequestHandler)
        {
            _logErrorRequestHandler = logErrorRequestHandler;
            _getAllErrorLogsRequestHandler = getAllErrorLogsRequestHandler;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] HttpCreateErrorLogRequest request)
        {
            var clientId = JwtClaimsHelper.GetName(HttpContext);

            if (request.Severity != 1&& request.Severity != 2 && request.Severity != 3)
                return BadRequest("Severity must either be 1 (Critical), 2 (Major) or 3 (Minor)");

            return Ok(await _logErrorRequestHandler.HandleRequest(new LogErrorRequest
            {
                ClientId = clientId,
                DateTimeOfOccurrence = request.DateTimeOfOccurrence,
                Severity = (Severity)request.Severity, 
                StackTrace = request.StackTrace,
                Summary = request.Summary,
                Identifier = request.Identifier,
                ObjectDump = request.ObjectDump
            }));
        }

        [HttpPost]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll([FromBody] HttpGetAllErrorLogsRequest request)
        {
            if (request.Severity != 1 && request.Severity != 2 && request.Severity != 3 && request.Severity != 0)
                return BadRequest("Severity must either be 1 (Critical), 2 (Major) or 3 (Minor)");

            return Ok(await _getAllErrorLogsRequestHandler.HandleRequest(new GetAllErrorLogsRequest
            {
                ClientId = request.ClientId,
                LowerBoundDateTime = request.LowerBoundDateTime, 
                UpperBoundDateTime = request.UpperBoundDateTime,
                Identifier = request.Identifier,
                IdentifierExact = request.IdentifierExact,
                Severity = (Severity)request.Severity,
                Summary = request.Summary,
                SummaryExact = request.SummaryExact,
                StackTrace = request.StackTrace,
                StackTraceExact = request.StackTraceExact
            }));
        }
    }
}
