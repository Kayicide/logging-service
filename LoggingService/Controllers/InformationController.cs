﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Controllers.HttpRequest;
using LoggingService.Data.Entity;
using LoggingService.Features;
using LoggingService.Features.InformationFeature;
using LoggingService.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace LoggingService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class InformationController : Controller
    {
        private readonly IRequestHandler<LogInfoRequest, ReturnObject> _createInfoLogRequestHandler;
        private readonly IRequestHandler<GetAllInfoLogsRequest, IList<Information>> _getAllInfoLogRequestHandler;
        public InformationController(IRequestHandler<LogInfoRequest, ReturnObject> createInfoLogRequestHandler, IRequestHandler<GetAllInfoLogsRequest, IList<Information>> getAllInfoLogRequestHandler)
        {
            _createInfoLogRequestHandler = createInfoLogRequestHandler;
            _getAllInfoLogRequestHandler = getAllInfoLogRequestHandler;
        }

        [HttpPost]
        public async Task<IActionResult> Create(HttpCreateInfoLogRequest request)
        {
            var clientId = JwtClaimsHelper.GetName(HttpContext);

            return Ok(await _createInfoLogRequestHandler.HandleRequest(new LogInfoRequest
            {
                ClientId = clientId,
                DateTimeOfOccurrence = request.DateTimeOfOccurrence,
                Identifier = request.Identifier,
                Summary = request.Summary,
                InfoText = request.InfoText,
                ObjectDump = request.ObjectDump
            }));
        }

        [HttpPost]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll(HttpGetAllInfoLogsRequest request)
        {
            var clientId = JwtClaimsHelper.GetName(HttpContext);

            return Ok(await _getAllInfoLogRequestHandler.HandleRequest(new GetAllInfoLogsRequest
            {
                ClientId = request.ClientId,
                LowerBoundDateTime = request.LowerBoundDateTime,
                UpperBoundDateTime = request.UpperBoundDateTime,
                Identifier = request.Identifier,
                IdentifierExact = request.IdentifierExact,
                Summary = request.Summary,
                SummaryExact = request.SummaryExact,
                InfoText = request.InfoText,
                InfoTextExact = request.InfoTextExact
            }));
        }
    }
}
