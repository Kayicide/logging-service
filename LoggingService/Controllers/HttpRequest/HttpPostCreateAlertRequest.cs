﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LoggingService.Controllers.HttpRequest
{
    public class HttpPostCreateAlertRequest
    {
        [Required] 
        public string ClientId { get; set; } = "";
        [Required]
        public string Name { get; set; }

        public string LogType { get; set; } = "";
        public string LogIdentifier { get; set; } = "";

        [Required]
        public int TimeSpan { get; set; }
        [Required]
        public int ThresholdAmount { get; set; }
    }
}
