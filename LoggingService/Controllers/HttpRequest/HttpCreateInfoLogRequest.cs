﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace LoggingService.Controllers.HttpRequest
{
    public class HttpCreateInfoLogRequest
    {
        //Required
        [Required]
        public DateTime DateTimeOfOccurrence { get; init; }
        [Required]
        public string Summary { get; init; }

        //Not required
        public string Identifier { get; init; } = string.Empty;
        public string InfoText { get; init; } = string.Empty;
        public JObject ObjectDump { get; set; } = null;
    }
}
