﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;

namespace LoggingService.Controllers.HttpRequest
{
    public class HttpGetAllErrorLogsRequest
    {
        public string ClientId { get; set; } = string.Empty;
        //Date Filter
        public DateTime LowerBoundDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperBoundDateTime { get; set; } = DateTime.MinValue;
        //Identifier Filter
        public string Identifier { get; init; } = string.Empty;

        public bool IdentifierExact { get; set; } = false;
        //Error Log Filters
        public string Summary { get; init; } = string.Empty;
        public bool SummaryExact { get; init; } = false;
        public int Severity { get; init; } = 0;
        public string StackTrace { get; init; } = string.Empty;
        public bool StackTraceExact { get; init; } = false;
    }
}
