﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LoggingService.Controllers.HttpRequest
{
    public class HttpPostUpdateAlertRequest
    {
        public string AlertId { get; set; }
        public string Name { get; set; }
        public string LogType { get; set; }
        public string LogIdentifier { get; set; }
        public int TimeSpan { get; set; }
        public int ThresholdAmount { get; set; }
        public bool? Deleted { get; set; }
    }
}
