﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using Newtonsoft.Json.Linq;

namespace LoggingService.Controllers.HttpRequest
{
    public class HttpCreateErrorLogRequest
    {
        //Required
        [Required]
        public DateTime DateTimeOfOccurrence { get; set; }
        [Required]
        public string Summary { get; init; }
        [Required]
        public int Severity { get; init; }

        //Not required
        public string StackTrace { get; init; } = "";
        public string Identifier { get; set; } = "";
        public JObject ObjectDump { get; set; } = null;

    }
}
