﻿using System;

namespace LoggingService.Controllers.HttpRequest
{
    public class HttpGetAllLogsRequest
    {
        public string ClientId { get; set; } = string.Empty;
        //Date Filter
        public DateTime LowerBoundDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperBoundDateTime { get; set; } = DateTime.MinValue;
        //Identifier Filter
        public string Summary { get; init; } = string.Empty;
        public bool SummaryExact { get; init; } = false;
        public string Identifier { get; init; } = string.Empty;
        public bool IdentifierExact { get; set; } = false;
    }
}
