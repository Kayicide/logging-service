﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoggingService.Controllers.HttpRequest
{
    public class HttpGetAllWarningLogsRequest
    {

        public string ClientId { get; set; } = string.Empty;
        public DateTime LowerBoundDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperBoundDateTime { get; set; } = DateTime.MinValue;
        public string Identifier { get; init; } = string.Empty;
        public bool IdentifierExact { get; set; } = false;
        public string Summary { get; set; } = string.Empty;
        public bool SummaryExact { get; set; } = false;
        public string WarningText { get; set; } = string.Empty;
        public bool WarningTextExact { get; set; } = false;
    }
}
