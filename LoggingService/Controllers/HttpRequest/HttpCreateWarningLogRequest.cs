﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace LoggingService.Controllers.HttpRequest
{
    public class HttpCreateWarningLogRequest
    {
        //Required
        [Required]
        public DateTime DateTimeOfOccurrence { get; set; }
        [Required]
        public string Summary { get; init; }

        //Not required
        public string Identifier { get; set; } = string.Empty;
        public string WarningText { get; set; } = string.Empty;
        public JObject ObjectDump { get; set; } = null;
    }
}
