﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Controllers.HttpRequest;
using LoggingService.Data.Entity;
using LoggingService.Features;
using LoggingService.Features.InformationFeature;
using LoggingService.Features.WarningFeature;
using LoggingService.Utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LoggingService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class WarningController : Controller
    {
        private readonly IRequestHandler<GetAllWarningLogsRequest, IList<Warning>> _getAllWarningLogsRequestHandler;
        private readonly IRequestHandler<LogWarningRequest, ReturnObject> _logWarningRequestHandler;

        public WarningController(
            IRequestHandler<GetAllWarningLogsRequest, IList<Warning>> getAllWarningLogsRequestHandler,
            IRequestHandler<LogWarningRequest, ReturnObject> logWarningRequestHandler)
        {
            _getAllWarningLogsRequestHandler = getAllWarningLogsRequestHandler;
            _logWarningRequestHandler = logWarningRequestHandler;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] HttpCreateWarningLogRequest request)
        {
            var clientId = JwtClaimsHelper.GetName(HttpContext);

            return Ok(await _logWarningRequestHandler.HandleRequest(new LogWarningRequest
            {
                Identifier = request.Identifier,
                ObjectDump = request.ObjectDump,
                ClientId = clientId,
                Summary = request.Summary,
                WarningText = request.WarningText,
                DateTimeOfOccurrence = request.DateTimeOfOccurrence
            }));
        }

        [HttpPost]
        [Route("GetAll")]
        public async Task<IActionResult> GetAll([FromBody] HttpGetAllWarningLogsRequest request)
        {
            return Ok(await _getAllWarningLogsRequestHandler.HandleRequest(new GetAllWarningLogsRequest
            {
                ClientId = request.ClientId,
                LowerBoundDateTime = request.LowerBoundDateTime,
                UpperBoundDateTime = request.UpperBoundDateTime,
                Identifier = request.Identifier,
                IdentifierExact = request.IdentifierExact,
                Summary = request.Summary,
                SummaryExact = request.SummaryExact,
                WarningText = request.WarningText, 
                WarningTextExact = request.WarningTextExact
            }));
        }

    }
}
