﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Controllers.HttpRequest;
using LoggingService.Data.Entity;
using LoggingService.Features;
using LoggingService.Features.LogFeature;
using LoggingService.Utility;
using Microsoft.AspNetCore.Authorization;

namespace LoggingService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class LogController : ControllerBase
    {
        private readonly IRequestHandler<GetAllLogsRequest, IList<Log>> _getAllLogsRequestHandler;
        public LogController(IRequestHandler<GetAllLogsRequest, IList<Log>> getAllLogsRequestHandler)
        {
            _getAllLogsRequestHandler = getAllLogsRequestHandler;
        }

        [HttpPost]
        public async Task<IActionResult> GetAll([FromBody] HttpGetAllLogsRequest request)
        {
            var logs = await _getAllLogsRequestHandler.HandleRequest(new GetAllLogsRequest
            {
                ClientId = request.ClientId,
                Identifier = request.Identifier,
                IdentifierExact = request.IdentifierExact,
                Summary =  request.Summary,
                SummaryExact = request.SummaryExact,
                LowerBoundDateTime = request.LowerBoundDateTime,
                UpperBoundDateTime = request.UpperBoundDateTime,
            });
            return Ok(logs);
        }
    }
}
