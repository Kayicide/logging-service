﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;

namespace LoggingService.Features.ClientFeature
{
    public class GetClientByClientIdRequest
    {
        public string ClientId { get; set; }
    }
    public class GetClientByClientIdRequestHandler : IRequestHandler<GetClientByClientIdRequest, Client>
    {
        private readonly IRepository<Client> _clientRepository;
        public GetClientByClientIdRequestHandler(IRepository<Client> clientRepository)
        {
            _clientRepository = clientRepository;
        }
        public async Task<Client> HandleRequest(GetClientByClientIdRequest request)
        {
            return await _clientRepository.Find(request.ClientId);
        }
    }
}
