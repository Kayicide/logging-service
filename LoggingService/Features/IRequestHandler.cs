﻿using System;
using System.Threading.Tasks;

namespace LoggingService.Features
{
    public interface IRequestHandler<in IRequest, TReturn>
    {
        public Task<TReturn> HandleRequest(IRequest request);
    }
}
