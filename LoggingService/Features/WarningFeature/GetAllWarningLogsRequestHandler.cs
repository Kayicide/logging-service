﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;

namespace LoggingService.Features.WarningFeature
{
    public class GetAllWarningLogsRequest : IRequest
    {
        public string ClientId { get; set; }
        public DateTime LowerBoundDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperBoundDateTime { get; set; } = DateTime.MinValue;
        public string Identifier { get; init; } = string.Empty;
        public bool IdentifierExact { get; set; } = false;
        public string Summary { get; set; } = string.Empty;
        public bool SummaryExact { get; set; } = false;
        public string WarningText { get; set; } = string.Empty;
        public bool WarningTextExact { get; set; } = false;
    }
    public class GetAllWarningLogsRequestHandler : IRequestHandler<GetAllWarningLogsRequest, IList<Warning>>
    {
        private readonly IRepository<Warning> _warningRepository;
        public GetAllWarningLogsRequestHandler(IRepository<Warning> warningRepository)
        {
            _warningRepository = warningRepository;
        }
        public async Task<IList<Warning>> HandleRequest(GetAllWarningLogsRequest request)
        {
            var logs = await _warningRepository.Read();

            logs = logs.Where(x => x.ClientId.Equals(request.ClientId, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (request.Identifier != string.Empty)
                logs = request.IdentifierExact ? logs.Where(x => x.Identifier.Equals(request.Identifier, StringComparison.InvariantCultureIgnoreCase)).ToList() : logs.Where(x => x.Identifier.Contains(request.Identifier, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (request.LowerBoundDateTime != DateTime.MinValue)
                logs = logs.Where(x => x.DateTimeOfOccurrence >= request.LowerBoundDateTime).ToList();
            if (request.UpperBoundDateTime != DateTime.MinValue)
                logs = logs.Where(x => x.DateTimeOfOccurrence <= request.UpperBoundDateTime).ToList();

            if (request.Summary != string.Empty)
                logs = request.SummaryExact ? logs.Where(x => x.Summary.Equals(request.Summary, StringComparison.InvariantCultureIgnoreCase)).ToList() : logs.Where(x => x.Summary.Contains(request.Summary)).ToList();
            
            if(request.WarningText != string.Empty)
                logs = request.WarningTextExact ? logs.Where(x => x.WarningText.Equals(request.WarningText, StringComparison.InvariantCultureIgnoreCase)).ToList() : logs.Where(x => x.WarningText.Contains(request.WarningText)).ToList();

            return logs;
        }
    }
}
