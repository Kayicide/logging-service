﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using LoggingService.Features.AlertFeature;
using LoggingService.Utility;
using Newtonsoft.Json.Linq;

namespace LoggingService.Features.WarningFeature
{
    public class LogWarningRequest : IRequest
    {
        public string ClientId { get; set; }
        public DateTime DateTimeOfOccurrence { get; set; }
        public string Identifier { get; set; }
        public string Summary { get; set; }
        public JObject ObjectDump { get; set; }
        public string WarningText { get; set; }
    }
    public class LogWarningRequestHandler : IRequestHandler<LogWarningRequest, ReturnObject>
    {
        private readonly IRepository<Warning> _warningRepository;
        private readonly IRequestHandler<CheckAndTriggerAlertRequest, bool> _checkAndTriggerAlertRequestHandler;
        public LogWarningRequestHandler(IRepository<Warning> warningRepository, IRequestHandler<CheckAndTriggerAlertRequest, bool> checkAndTriggerAlertRequestHandler)
        {
            _warningRepository = warningRepository;
            _checkAndTriggerAlertRequestHandler = checkAndTriggerAlertRequestHandler;
        }
        public async Task<ReturnObject> HandleRequest(LogWarningRequest request)
        {
            Warning warning = new(request.ClientId, request.Summary, request.DateTimeOfOccurrence)
            {
                Identifier = request.Identifier,
                ObjectDump = request.ObjectDump.ToString(),
                WarningText = request.WarningText,
            };

            await _warningRepository.Create(warning);
            await _checkAndTriggerAlertRequestHandler.HandleRequest(new CheckAndTriggerAlertRequest{ClientId = request.ClientId});
            return new ReturnObject{Success = true};
        }
    }
}
