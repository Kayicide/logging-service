﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using LoggingService.Utility;
using Microsoft.IdentityModel.Tokens;

namespace LoggingService.Features.Authentication
{
    public class GenerateJwtForClientRequest
    {
        public string ClientId { get; set; }
    }
    public class GenerateJwtForClientRequestHandler : IRequestHandler<GenerateJwtForClientRequest, Client>
    {
        private readonly AppSettings _appSettings;
        private readonly IRepository<Client> _clientRepository;

        public GenerateJwtForClientRequestHandler(AppSettings appSettings, IRepository<Client> clientRepository)
        {
            _appSettings = appSettings;
            _clientRepository = clientRepository;
        }

        public async Task<Client> HandleRequest(GenerateJwtForClientRequest request)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(JwtRegisteredClaimNames.UniqueName, request.ClientId),
                    new Claim(ClaimTypes.Role, "Client")
                }),
                Expires = DateTime.UtcNow.AddYears(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            //updates jwt token in the client
            var client = await _clientRepository.Find(request.ClientId);
            client.ApiToken = tokenString;
            await _clientRepository.Update(client);

            return client;
        }
    }
}
