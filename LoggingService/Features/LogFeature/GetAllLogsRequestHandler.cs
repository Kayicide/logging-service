﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;

namespace LoggingService.Features.LogFeature

{
    public class GetAllLogsRequest : IRequest
    {
        public string ClientId { get; set; }
        public DateTime LowerBoundDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperBoundDateTime { get; set; } = DateTime.MinValue;
        public string Summary { get; init; } = string.Empty;
        public bool SummaryExact { get; init; } = false;
        public string Identifier { get; init; } = string.Empty;
        public bool IdentifierExact { get; set; } = false;
    }
    public class GetAllLogsRequestHandler : IRequestHandler<GetAllLogsRequest, IList<Log>>
    {
        private readonly IRepository<Log> _logRepository;
        public GetAllLogsRequestHandler(IRepository<Log> logRepository)
        {
            _logRepository = logRepository;
        }

        public async Task<IList<Log>> HandleRequest(GetAllLogsRequest request)
        {
            var logs = await _logRepository.Read();
            logs = logs.Where(x => x.ClientId.Equals(request.ClientId, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if(request.Identifier != string.Empty)
                logs = request.IdentifierExact ? logs.Where(x => x.Identifier.Equals(request.Identifier, StringComparison.InvariantCultureIgnoreCase)).ToList() : logs.Where(x => x.Identifier.Contains(request.Identifier, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (request.Summary != string.Empty)
                logs = request.SummaryExact ? logs.Where(x => x.Summary.Equals(request.Summary, StringComparison.InvariantCultureIgnoreCase)).ToList() : logs.Where(x => x.Summary.Contains(request.Summary, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (request.LowerBoundDateTime != DateTime.MinValue)
                logs = logs.Where(x => x.DateTimeOfOccurrence >= request.LowerBoundDateTime).ToList();
            if (request.UpperBoundDateTime != DateTime.MinValue)
                logs = logs.Where(x => x.DateTimeOfOccurrence <= request.UpperBoundDateTime).ToList();

            return logs;
        }
    }
}
