﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using MongoDB.Bson;

namespace LoggingService.Features.AlertFeature
{
    public class GetAllAlertsRequest : IRequest
    {
        public string ClientId { get; set; }
    }
    public class GetAllAlertsRequestHandler : IRequestHandler<GetAllAlertsRequest, IList<Alert>>
    {
        private readonly IRepository<Alert> _alertRepository;
        public GetAllAlertsRequestHandler(IRepository<Alert> alertRepository)
        {
            _alertRepository = alertRepository;
        }
        public async Task<IList<Alert>> HandleRequest(GetAllAlertsRequest request)
        {
            var alerts = await _alertRepository.Read();
            return alerts.Where(x => x.ClientId.Equals(request.ClientId, StringComparison.InvariantCultureIgnoreCase) && x.Deleted == false).ToList();
        }
    }
}
