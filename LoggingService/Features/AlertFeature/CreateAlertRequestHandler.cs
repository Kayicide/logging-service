﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using MongoDB.Bson;

namespace LoggingService.Features.AlertFeature
{
    public class CreateAlertRequest : IRequest
    {
        public string ClientId { get; set; }
        public string Name { get; set; }
        public string LogType { get; set; }
        public string LogIdentifier { get; set; }
        public int TimeSpan { get; set; }
        public int ThresholdAmount { get; set; }
    }
    public class CreateAlertRequestHandler : IRequestHandler<CreateAlertRequest, Alert>
    {
        private readonly IRepository<Alert> _alertRepository;
        private readonly IRepository<Client> _clientRepository;
        public CreateAlertRequestHandler(IRepository<Alert> alertRepository, IRepository<Client> clientRepository)
        {
            _alertRepository = alertRepository;
            _clientRepository = clientRepository;
        }
        public async Task<Alert> HandleRequest(CreateAlertRequest request)
        {
            var client = await _clientRepository.Find(request.ClientId);
            if (client == null)
                return null;

            var newAlert = new Alert
            {
                Id = ObjectId.GenerateNewId().ToString(),
                ClientId = request.ClientId,
                Name = request.Name,
                LogIdentifier = request.LogIdentifier,
                TimeSpan = request.TimeSpan,
                LogType = request.LogType,
                ThresholdAmount = request.ThresholdAmount,
                CreatedDateTime = DateTime.Now
            };

            await _alertRepository.Create(newAlert);

            return newAlert;
        }
    }
}
