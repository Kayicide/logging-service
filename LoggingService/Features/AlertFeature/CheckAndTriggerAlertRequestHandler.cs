﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using LoggingService.Features.LogFeature;
using LoggingService.Utility;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace LoggingService.Features.AlertFeature
{
    public class CheckAndTriggerAlertRequest : IRequest
    {
        public string ClientId { get; set; }
    }
    public class CheckAndTriggerAlertRequestHandler : IRequestHandler<CheckAndTriggerAlertRequest, bool>
    {
        private readonly IRepository<Alert> _alertRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRequestHandler<GetAllLogsRequest, IList<Log>> _getAllLogsRequestHandler;
        private readonly AppSettings _appSettings;

        public CheckAndTriggerAlertRequestHandler(IRepository<Alert> alertRepository, IRepository<User> userRepository, IRequestHandler<GetAllLogsRequest, IList<Log>> getAllLogsRequestHandler, AppSettings appSettings)
        {
            _alertRepository = alertRepository;
            _userRepository = userRepository;   
            _getAllLogsRequestHandler = getAllLogsRequestHandler;
            _appSettings = appSettings;
        }
        public async Task<bool> HandleRequest(CheckAndTriggerAlertRequest request)
        { 
            var dateTimeNow = DateTime.Now;
            var users = (await _userRepository.Read()).Where(x => x.ClientIds.Contains(request.ClientId)).ToList();
            var alerts = (await _alertRepository.Read()).Where(x => x.ClientId.Equals(request.ClientId, StringComparison.InvariantCultureIgnoreCase) && x.Deleted == false).ToList();
            foreach (var alert in alerts)
            {
                var logs = await GetLogs(alert, dateTimeNow, request.ClientId);
                if (logs.Count >= alert.ThresholdAmount)
                {
                    if (alert.LastTriggered < dateTimeNow.AddMinutes(-alert.TimeSpan))
                    {
                        alert.LastTriggered = dateTimeNow;
                        await _alertRepository.Update(alert);
                        await SendEmailToUsers(users, alert, logs.Count);
                    }
                }
            }

            return true;
        }
        private async Task<IList<Log>> GetLogs(Alert alert, DateTime now, string clientId)
        {
            var logs = await _getAllLogsRequestHandler.HandleRequest(new GetAllLogsRequest { ClientId = clientId, LowerBoundDateTime = now.AddMinutes(-alert.TimeSpan), Identifier = alert.LogIdentifier });
            switch (alert.LogType)
            {
                case "Error":
                    return logs.Where(x => x.GetType() == typeof(Error)).ToList();
                case "Information":
                    return logs.Where(x => x.GetType() == typeof(Information)).ToList();
                case "Warning":
                    return logs.Where(x => x.GetType() == typeof(Warning)).ToList();
                default:
                    return logs;
            }
        }

        public async Task SendEmailToUsers(IList<User> users, Alert alert, int numberOfLogs)
        {

            var client = new SendGridClient(_appSettings.MailGridAuthKey);
            var from = new EmailAddress("Scarlett@Kayde.me", "LogMaster System");
            var subject = $"{alert.Name} Alert Triggered!";
            var plainTextContent = $"The alert: {alert.Name} has been triggered with {numberOfLogs} {alert.LogType}'s being logged in the last {alert.TimeSpan} minutes!";
            var htmlContent = $"<h>{alert.Name} Triggered!</h><p>{alert.Name} has been triggered with {numberOfLogs} {alert.LogType}'s being logged in the last {alert.TimeSpan} minutes!</p>";
            foreach (var user in users)
            {
                var to = new EmailAddress(user.Email, user.FirstName + " " + user.LastName);
                await client.SendEmailAsync(MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent));
            }

        }
    }
}
