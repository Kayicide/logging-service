﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;

namespace LoggingService.Features.AlertFeature
{
    public class UpdateAlertRequest : IRequest
    {
        public string AlertId { get; set; }
        public string Name { get; set; }
        public string LogType { get; set; }
        public string LogIdentifier { get; set; }
        public int TimeSpan { get; set; }
        public int ThresholdAmount { get; set; }
        public bool? Deleted { get; set; }
    }
    public class UpdateAlertRequestHandler : IRequestHandler<UpdateAlertRequest, Alert>
    {
        private readonly IRepository<Alert> _alertRepository;
        public UpdateAlertRequestHandler(IRepository<Alert> alertRepository)
        {
            _alertRepository = alertRepository;
        }
        public async Task<Alert> HandleRequest(UpdateAlertRequest request)
        {
            var alert = await _alertRepository.Find(request.AlertId);
            if (alert == null)
                return null;

            alert.Name = request.Name;
            alert.LogType = request.LogType;
            alert.LogIdentifier = request.LogIdentifier;
            alert.TimeSpan = request.TimeSpan;
            alert.ThresholdAmount = request.ThresholdAmount;
            if (request.Deleted.HasValue)
                alert.Deleted = request.Deleted.Value;

            await _alertRepository.Update(alert);
            return alert;
        }
    }
}
