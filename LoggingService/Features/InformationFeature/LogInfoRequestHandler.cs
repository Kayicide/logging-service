﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using LoggingService.Features.AlertFeature;
using LoggingService.Utility;
using Newtonsoft.Json.Linq;

namespace LoggingService.Features.InformationFeature
{
    public class LogInfoRequest : IRequest
    {
        public string ClientId { get; set; }
        public DateTime DateTimeOfOccurrence { get; set; }
        public string Summary { get; set; }
        public string Identifier { get; set; }
        public string InfoText { get; set; }
        public JObject ObjectDump { get; set; }
    }
    public class LogInfoRequestHandler : IRequestHandler<LogInfoRequest, ReturnObject>
    {
        private readonly IRepository<Information> _infoRepository;
        private readonly IRequestHandler<CheckAndTriggerAlertRequest, bool> _checkAndTriggerAlertRequestHandler;
        public LogInfoRequestHandler(IRepository<Information> infoRepository, IRequestHandler<CheckAndTriggerAlertRequest, bool> checkAndTriggerAlertRequestHandler)
        {
            _infoRepository = infoRepository;
            _checkAndTriggerAlertRequestHandler = checkAndTriggerAlertRequestHandler;
        }
        public async Task<ReturnObject> HandleRequest(LogInfoRequest request)
        {
            var newInfo = new Information(request.ClientId, request.Summary, request.DateTimeOfOccurrence)
            {
                Identifier = request.Identifier, 
                InfoText = request.InfoText,
                ObjectDump = request.ObjectDump.ToString()
            };

            await _infoRepository.Create(newInfo);
            await _checkAndTriggerAlertRequestHandler.HandleRequest(new CheckAndTriggerAlertRequest { ClientId = request.ClientId });
            return new ReturnObject { Success = true };
        }
    }
}
