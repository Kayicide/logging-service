﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;

namespace LoggingService.Features.InformationFeature
{
    public class GetAllInfoLogsRequest : IRequest
    {
        public string ClientId { get; set; }
        //Date Filter
        public DateTime LowerBoundDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperBoundDateTime { get; set; } = DateTime.MinValue;
        //Identifier Filter
        public string Identifier { get; init; } = string.Empty;
        public bool IdentifierExact { get; init; } = false;
        //Error Log Filters
        public string Summary { get; init; } = string.Empty;
        public bool SummaryExact { get; init; } = false;
        public string InfoText { get; set; } = string.Empty;
        public bool InfoTextExact { get; set; } = false;
    }
    public class GetAllInfoLogsRequestHandler : IRequestHandler<GetAllInfoLogsRequest, IList<Information>>
    {
        private readonly IRepository<Information> _infoRepository;
        public GetAllInfoLogsRequestHandler(IRepository<Information> infoRepository)
        {
            _infoRepository = infoRepository;
        }
        public async Task<IList<Information>> HandleRequest(GetAllInfoLogsRequest request)
        {
            var logs = await _infoRepository.Read();

            //Normal Log Filtering

            logs = logs.Where(x => x.ClientId.Equals(request.ClientId, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (request.Identifier != string.Empty)
                logs = request.IdentifierExact ? logs.Where(x => x.Identifier.Equals(request.Identifier, StringComparison.InvariantCultureIgnoreCase)).ToList() : logs.Where(x => x.Identifier.Contains(request.Identifier, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (request.LowerBoundDateTime != DateTime.MinValue)
                logs = logs.Where(x => x.DateTimeOfOccurrence >= request.LowerBoundDateTime).ToList();
            if (request.UpperBoundDateTime != DateTime.MinValue)
                logs = logs.Where(x => x.DateTimeOfOccurrence <= request.UpperBoundDateTime).ToList();

            //Info Log Filtering
            if (request.Summary != string.Empty)
                logs = request.SummaryExact ? logs.Where(x => x.Summary.Equals(request.Summary, StringComparison.InvariantCultureIgnoreCase)).ToList() : logs.Where(x => x.Summary.Contains(request.Summary)).ToList();
            if (request.InfoText != string.Empty)
                logs = request.InfoTextExact ? logs.Where(x => x.InfoText.Equals(request.InfoText, StringComparison.InvariantCultureIgnoreCase)).ToList() : logs.Where(x => x.InfoText.Contains(request.InfoText)).ToList();

            return logs;
        } 
    }
}
