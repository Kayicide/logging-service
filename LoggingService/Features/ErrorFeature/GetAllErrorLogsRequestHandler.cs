﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;

namespace LoggingService.Features.ErrorFeature
{
    public class GetAllErrorLogsRequest : IRequest
    {
        public string ClientId { get; set; }
        //Date Filter
        public DateTime LowerBoundDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperBoundDateTime { get; set; } = DateTime.MinValue;
        //Identifier Filter
        public string Identifier { get; init; } = string.Empty;

        public bool IdentifierExact { get; set; } = false;
        //Error Log Filters
        public string Summary { get; set; } = string.Empty;
        public bool SummaryExact { get; set; } = false;
        public Severity Severity { get; set; } = 0;
        public string StackTrace { get; set; } = string.Empty;
        public bool StackTraceExact { get; set; } = false;
    }
    public class GetAllErrorLogsRequestHandler : IRequestHandler<GetAllErrorLogsRequest, IList<Error>>
    {
        private readonly IRepository<Error> _errorRepository;
        public GetAllErrorLogsRequestHandler(IRepository<Error> errorRepository)
        {
            _errorRepository = errorRepository;
        }
        public async Task<IList<Error>> HandleRequest(GetAllErrorLogsRequest request)
        {
            var logs = await _errorRepository.Read();

            //Normal Log Filtering

            logs = logs.Where(x => x.ClientId.Equals(request.ClientId, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (request.Identifier != string.Empty)
                logs = request.IdentifierExact ? logs.Where(x => x.Identifier.Equals(request.Identifier, StringComparison.InvariantCultureIgnoreCase)).ToList() : logs.Where(x => x.Identifier.Contains(request.Identifier, StringComparison.InvariantCultureIgnoreCase)).ToList();

            if (request.LowerBoundDateTime != DateTime.MinValue)
                logs = logs.Where(x => x.DateTimeOfOccurrence >= request.LowerBoundDateTime).ToList();
            if (request.UpperBoundDateTime != DateTime.MinValue)
                logs = logs.Where(x => x.DateTimeOfOccurrence <= request.UpperBoundDateTime).ToList();

            //Error Log Filtering
            if(request.Summary != string.Empty) 
                logs = request.SummaryExact ? logs.Where(x => x.Summary.Equals(request.Summary, StringComparison.InvariantCultureIgnoreCase)).ToList() : logs.Where(x => x.Summary.Contains(request.Summary)).ToList();
            if(request.StackTrace != string.Empty)
                logs = request.StackTraceExact ? logs.Where(x => x.StackTrace.Equals(request.StackTrace, StringComparison.InvariantCultureIgnoreCase)).ToList() : logs.Where(x => x.StackTrace.Contains(request.StackTrace)).ToList();

            if (request.Severity != 0)
                logs = logs.Where(x => x.Severity == request.Severity).ToList();

            return logs;
        }
    }
}
