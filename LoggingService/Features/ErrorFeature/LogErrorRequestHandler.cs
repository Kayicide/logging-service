﻿using System;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using LoggingService.Data.Repositories;
using LoggingService.Features.AlertFeature;
using LoggingService.Utility;
using Newtonsoft.Json.Linq;

namespace LoggingService.Features.ErrorFeature
{
    public class LogErrorRequest : IRequest
    {
        public string ClientId { get; set; }
        public DateTime DateTimeOfOccurrence { get; set; }
        public string Identifier { get; set; }
        public string Summary { get; set; }
        public Severity Severity { get; set; }
        public string StackTrace { get; set; }
        public JObject ObjectDump { get; set; }

    }
    public class LogErrorRequestHandler : IRequestHandler<LogErrorRequest, ReturnObject>
    {
        private readonly IRepository<Error> _errorRepository;
        private readonly IRequestHandler<CheckAndTriggerAlertRequest, bool> _checkAndTriggerAlertRequestHandler;
        public LogErrorRequestHandler(IRepository<Error> errorRepository, IRequestHandler<CheckAndTriggerAlertRequest, bool> checkAndTriggerAlertRequestHandler)
        {
            _errorRepository = errorRepository;
            _checkAndTriggerAlertRequestHandler = checkAndTriggerAlertRequestHandler;
        }

        public async Task<ReturnObject> HandleRequest(LogErrorRequest request)
        {
            var newError = new Error(request.ClientId, request.Summary, request.DateTimeOfOccurrence)
            {
                Identifier = request.Identifier,
                Severity = request.Severity,
                StackTrace = request.StackTrace,
                ObjectDump = request.ObjectDump.ToString()
            };
            await _errorRepository.Create(newError);
            await _checkAndTriggerAlertRequestHandler.HandleRequest(new CheckAndTriggerAlertRequest{ClientId = request.ClientId});
            return new ReturnObject{Success = true};
        }
    }
}
