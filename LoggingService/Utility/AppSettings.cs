﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoggingService.Utility
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string FrontEndName { get; set; }
        public string MailGridAuthKey { get; set; }
    }
}
