﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace LoggingService.Utility
{
    public static class JwtClaimsHelper
    {
        public static string GetName(HttpContext context)
        {
            var identity = context.User.Claims;
            return identity.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
        }

        public static string GetRole(HttpContext context)
        {
            var identity = context.User.Claims;
            return identity.FirstOrDefault(x => x.Type == ClaimTypes.Role)?.Value;
        }
    }
}
