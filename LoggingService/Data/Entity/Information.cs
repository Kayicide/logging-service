﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoggingService.Data.Entity
{
    public class Information : Log
    {
        public Information(string clientId, string summary, DateTime dateTimeOfOccurrence) : base(clientId, summary, dateTimeOfOccurrence) { }
        public string InfoText { get; set; }
    }
}
