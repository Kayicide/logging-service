﻿using System.Collections.Generic;
using LoggingService.Utility;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace LoggingService.Data.Entity
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public IList<string> ClientIds { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public SaltedHash.HashSalt HashSalt { get; set; }
        public bool Deleted { get; set; }
        public string Role { get; set; }
    }
}
