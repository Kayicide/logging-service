﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace LoggingService.Data.Entity
{
    public class Alert
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string ClientId { get; set; }
        public string Name { get; set; }
        public string LogType { get; set; }
        public string LogIdentifier { get; set; }
        public int TimeSpan { get; set; } //This can be 1, 10 60 mins
        public int ThresholdAmount { get; set; }
        public DateTime LastTriggered { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public bool Deleted { get; set; }
    }
}
