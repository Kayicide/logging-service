﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoggingService.Data.Entity
{
    public class Warning : Log
    {
        public Warning(string clientId, string summary, DateTime dateTimeOfOccurrence) : base(clientId, summary, dateTimeOfOccurrence)
        {
        }
        public string WarningText { get; set; }
    }
}
