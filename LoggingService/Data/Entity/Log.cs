﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace LoggingService.Data.Entity
{
    [BsonDiscriminator(RootClass = true)]
    [BsonKnownTypes(typeof(Error), typeof(Information))]
    public class Log
    {
        public Log(string clientId, string summary, DateTime dateTimeOfOccurrence)
        {
            ClientId = clientId;
            Summary = summary;
            DateTimeOfOccurrence = dateTimeOfOccurrence;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string LogId { get; set; }
        public string ClientId { get; init; }
        public string Identifier { get; init; }
        public string Summary { get; init; }
        public string ObjectDump { get; set; }
        public DateTime DateTimeOfOccurrence { get; init; }

    }
}
