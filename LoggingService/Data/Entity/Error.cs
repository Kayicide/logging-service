﻿using System;
namespace LoggingService.Data.Entity
{
    public enum Severity
    {
        Critical = 1,
        Major = 2, 
        Minor = 3
    }
    public class Error : Log
    {
        public Error(string clientId, string summary, DateTime dateTimeOfOccurrence) : base(clientId, summary, dateTimeOfOccurrence) { }
        public Severity Severity { get; init; }
        public string StackTrace { get; init; }
    }
}
