﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace LoggingService.Data
{
    public class DbAccessor : IDbAccessor
    {
        public IMongoDatabase Database => (new MongoClient(_settings.ConnectionString)).GetDatabase(_settings.DatabaseName);
        private readonly IDatabaseSettings _settings;
        public DbAccessor(IDatabaseSettings settings)
        {
            _settings = settings;
        }
    }
}
