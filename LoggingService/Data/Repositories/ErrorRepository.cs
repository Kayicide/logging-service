﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using MongoDB.Bson;
using MongoDB.Driver;

namespace LoggingService.Data.Repositories
{
    public class ErrorRepository : IRepository<Error>
    {
        private readonly IMongoCollection<Error> _errors;

        public ErrorRepository(IDbAccessor databaseAccessor)
        {
            _errors = databaseAccessor.Database.GetCollection<Error>("logs");
        }

        public async Task<Error> Create(Error error)
        {
            await _errors.InsertOneAsync(error);
            return error;
        }

        public async Task<IList<Error>> Read()
        {
            var filter = Builders<Error>.Filter.Eq("_t", "Error");
            return (await _errors.FindAsync(filter)).ToList();
        }

        public async Task<Error> Find(string id) =>
            (await _errors.FindAsync(aError => aError.LogId == id)).SingleOrDefault();

        public async Task Update(Error error) =>
            await _errors.ReplaceOneAsync(aError => aError.LogId == error.LogId, error);

        public async Task Delete(string id) =>
            await _errors.DeleteOneAsync(aError => aError.LogId == id);
    }
}
