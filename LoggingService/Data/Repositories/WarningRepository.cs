﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using MongoDB.Driver;

namespace LoggingService.Data.Repositories
{
    public class WarningRepository : IRepository<Warning>
    {
        private readonly IMongoCollection<Warning> _warnings;

        public WarningRepository(IDbAccessor databaseAccessor)
        {
            _warnings = databaseAccessor.Database.GetCollection<Warning>("logs");
        }

        public async Task<Warning> Create(Warning warning)
        {
            await _warnings.InsertOneAsync(warning);
            return warning;
        }

        public async Task<IList<Warning>> Read()
        {
            var filter = Builders<Warning>.Filter.Eq("_t", "Warning");
            return (await _warnings.FindAsync(filter)).ToList();
        }

        public async Task<Warning> Find(string id) =>
            (await _warnings.FindAsync(aWarning => aWarning.LogId == id)).SingleOrDefault();

        public async Task Update(Warning warning) =>
            await _warnings.ReplaceOneAsync(aWarning => aWarning.LogId == warning.LogId, warning);

        public async Task Delete(string id) =>
            await _warnings.DeleteOneAsync(aWarning => aWarning.LogId == id);
    }
}
