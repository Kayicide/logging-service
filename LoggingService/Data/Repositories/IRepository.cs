﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LoggingService.Data.Repositories
{
    public interface IRepository<T>
    {
        public Task<T> Create(T t);

        public Task<IList<T>> Read();

        public Task<T> Find(string id);

        public Task Update(T t);

        public Task Delete(string id);
    }
}
