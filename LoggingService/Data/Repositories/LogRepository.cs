﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using MongoDB.Driver;

namespace LoggingService.Data.Repositories
{
    public class LogRepository : IRepository<Log>
    {
        private readonly IMongoCollection<Log> _logs;

        public LogRepository(IDbAccessor databaseAccessor)
        {
            _logs = databaseAccessor.Database.GetCollection<Log>("logs");
        }

        public async Task<Log> Create(Log log)
        {
            await _logs.InsertOneAsync(log);
            return log;
        }

        public async Task<IList<Log>> Read() =>
            (await _logs.FindAsync(sub => true)).ToList();

        public async Task<Log> Find(string id) =>
            (await _logs.FindAsync(sub => sub.LogId == id)).SingleOrDefault();

        public async Task Update(Log log) =>
            await _logs.ReplaceOneAsync(aLog => aLog.LogId == log.LogId, log);

        public async Task Delete(string id) =>
            await _logs.DeleteOneAsync(aLog => aLog.LogId == id);
    }
}
