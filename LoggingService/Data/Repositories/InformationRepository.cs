﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using MongoDB.Driver;

namespace LoggingService.Data.Repositories
{
    public class InformationRepository : IRepository<Information>
    {
        private readonly IMongoCollection<Information> _infos;

        public InformationRepository(IDbAccessor databaseAccessor)
        {
            _infos = databaseAccessor.Database.GetCollection<Information>("logs");
        }

        public async Task<Information> Create(Information info)
        {
            await _infos.InsertOneAsync(info);
            return info;
        }

        public async Task<IList<Information>> Read()
        {
            var filter = Builders<Information>.Filter.Eq("_t", "Information");
            return (await _infos.FindAsync(filter)).ToList();
        }

        public async Task<Information> Find(string id) =>
            (await _infos.FindAsync(sub => sub.LogId == id)).SingleOrDefault();

        public async Task Update(Information info) =>
            await _infos.ReplaceOneAsync(aInfo => aInfo.LogId == info.LogId, info);

        public async Task Delete(string id) =>
            await _infos.DeleteOneAsync(aInfo => aInfo.LogId == id);
    }
}
