﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Data.Entity;
using MongoDB.Driver;

namespace LoggingService.Data.Repositories
{
    public class AlertRepository : IRepository<Alert>
    {

        private readonly IMongoCollection<Alert> _alerts;

        public AlertRepository(IDbAccessor databaseAccessor)
        {
            _alerts = databaseAccessor.Database.GetCollection<Alert>("alerts");
        }

        public async Task<Alert> Create(Alert error)
        {
            await _alerts.InsertOneAsync(error);
            return error;
        }

        public async Task<IList<Alert>> Read() =>
            (await _alerts.FindAsync(sub => true)).ToList();

        public async Task<Alert> Find(string id) =>
            (await _alerts.FindAsync(aAlert => aAlert.Id == id)).SingleOrDefault();

        public async Task Update(Alert alert) =>
            await _alerts.ReplaceOneAsync(aAlert => aAlert.Id == alert.Id, alert);

        public async Task Delete(string id) =>
            await _alerts.DeleteOneAsync(aAlert => aAlert.Id == id);
    }
}
