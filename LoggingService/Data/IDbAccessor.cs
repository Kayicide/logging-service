﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace LoggingService.Data
{
    public interface IDbAccessor
    {
        IMongoDatabase Database { get; }
    }
}
